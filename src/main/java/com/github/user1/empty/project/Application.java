package com.github.user1.empty.project;

import io.devbench.uibuilder.annotations.EnableUIBuilder;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

@EnableUIBuilder("com.github.user1.empty.project")
@SpringBootApplication(scanBasePackages = "com.github.user1.empty.project")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
